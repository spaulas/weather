City Weather
This app is a React application which displays a table with information about the city that was inserted by the user in a text box, with a maximum of three  at each time, only accepting english city names. It also displays a bar graph to more visually compare each city current temperature.

Installation
To properly execute this file, it is first necessary to install npm which comes with Node.js.

Execution
To execute this project, open a command line inside the project directory and enter the following commands:
	yarn add antd
	yarn add react-vis

And then
	yarn start
will start the application on the local host, opening a browser.

About the code - index.js
It is first introduced a Form, two Buttons and an empty Table.
The first button, 'Search', call a function when clicked that:
	- gets the cities ids through the cities names inserted, using a local json file, citylist.json (it is also possible to use the actual introduced city name for the api, but a city id is more accurate);
	- for every city id that is defined, it gets the temperature, sunrise time and sunset time by calling an api which retrieves all the information about the weather, those are saved inside a state variable (if a city name is misspelled, a message is shown to the user);
	- for every city it is also created a state variable which has the x (city name) and y (city temperature) for the bar graph;
	- after getting all the necessary information, it is displayed in the table, which has the possibility to order by name, temperature, sunrise time and sunset time, and in a graph;
The second button, clear, calls a function when clicked that resets the values introduced in Forms, consequentially leaving the table empty again and hiding the graph.