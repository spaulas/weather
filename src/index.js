import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import * as serviceWorker from './serviceWorker';

import CityIdData from "./data/citylist.json";

import { 
    Button,
    Input, 
    Form, 
    Table,
    notification
} from 'antd';

import {
    XYPlot,
    XAxis, // Shows the values on x axis
    YAxis, // Shows the values on y axis
    VerticalBarSeries,
    LabelSeries
} from 'react-vis';

const APIKEY = "7275fb049c9fc33a22669fc573cb41c6";
const APIURL = "http://api.openweathermap.org/data/2.5/weather?id=";

class Weather extends React.Component {
    
    constructor(props){
        super(props);
        
        this.state = {
            dataChart: [],
            city: [],
            showAlert: false
        };   
    }
    
    // ----------------------------------------
    // Functions for the Form:
    // Search Button
    handleSearch(){
        
        this.setState({
            city: [],
            dataChart: [],
            showAlert: false
        });
        
        // Get inputed values
        var cityName = [null, null, null];
            
        cityName[0] = this.state.input1;
        cityName[1] = this.state.input2;
        cityName[2] = this.state.input3;
        
        // Get city id from city name
        var cityId = [null, null, null];
        
        for(let i = 0; i < cityName.length; i++)
        {
            if(cityName[i]){
                cityId[i] = [this.getCityId(cityName[i])];
                cityId[i] && this.getWeather(cityId[i]);
            }
        }      
    }
    
    // Clear Button
    handleReset(){
        
        this.setState(
        {
            city: [],
            dataChart: [],
            input1:undefined,
            input2:undefined,
            input3:undefined,
            showAlert: false
        });
    }
    
    // ----------------------------------------
    // Getting city weather info functions
    // Get city id from city name using CityIdData.json
    getCityId(value){
        
        // Go through CityIdData to find a city with the same name has value
        var tmp =  CityIdData.filter(item => {
                    return item.name.toLowerCase() === value.toLowerCase();
                });
        
        // If a city was found then it returns its id
        if(tmp.length && tmp[0].hasOwnProperty("id") && tmp[0].id){
            return tmp[0].id;
        }
        
        else{
            return null;
        }
    }
    
    // Get weather info (name, temp, sunrise, sunset)
    getWeather = async(cityId) => {
        // fetch the api
        const api_call = await fetch(APIURL + cityId + "&appid=" + APIKEY);
        const response = await api_call.json();
        
        // check if the city has indeed content (cod = 200)
        if(response.cod === 200){
            
            // create auxiliary array with one element with the info of the city 
            const auxCity = [{
                name: response.name,
                temp: this.convertKelvinToCelsius(response.main.temp),
                sunrise: this.convertUnixTime(response.sys.sunrise),
                sunset: this.convertUnixTime(response.sys.sunset)
            }];
    
            // create auxiliary array with one element with the coordenates of the chart
            const auxDataChart = [{
                x: response.name,
                y: this.convertKelvinToCelsius(response.main.temp)
            }];
        
            // concatenate previous cities and dataCharts with the new ones on the state
            this.setState((prevState)=>({
                city:[...prevState.city,...auxCity],
                dataChart: [...prevState.dataChart,...auxDataChart]
            }));
        }
        
        // Without content (cod = '400')
        if(response.cod === '400'){
            this.setState({
                showAlert: true
            });
        }
        
        // set showAlert to false right away to avoid multiple notifications
        this.setState({
                showAlert: false
            });
    }
    
    // Converts Temperature from Kelvin to Celsius
    convertKelvinToCelsius(temp){
        temp = temp - 273.15;
        temp = temp.toFixed(2);
        return temp;
    }
    
    // Convert Unix time to String
    convertUnixTime(time){
        // Create a new JavaScript Date object based on the timestamp
        // convert to miliseconds
        var date = new Date(time*1000);
        // Hours part from the timestamp
        var hours = date.getHours();
        // Minutes part from the timestamp
        var minutes = "0" + date.getMinutes();
        // Seconds part from the timestamp
        var seconds = "0" + date.getSeconds();

        // Will display time in 10:30:23 format
        var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
        
        return formattedTime;
    }
    
    // ----------------------------------------
    // Functions for the Sorter
    
    compare (a, b){ 
        if (a > b) return -1; 
        if (a < b)  return 1;  
        
        return 0; 
    };

    compareTime(a,b){
        var aHour = a[0] + a[1];
        var bHour = b[0] + b[1];
        var aMin = a[3] + a[4];
        var bMin = b[3] + b[4];
        var aSeg = a[6] + a[7];
        var bSeg = b[6] + b[7];

        var compareHour = this.compare(aHour, bHour);
        var compareMin = this.compare(aMin, bMin);
        var compareSeg = this.compare(aSeg, bSeg);

        if(compareHour === 0){
            if(compareMin === 0){
                return compareSeg;
            }
            return compareMin;
        }

        return compareHour;
    }

    render(){
        
        // get constants of the state
        const{ dataChart, city, input1, input2, input3, showAlert}=this.state;
        
        // constant for the notification message
        const args = {
              message: 'City Name is Incorrect',
              description: 'A city name is not correct, please try again',
              duration: 4.5
        };
        
        // constant for the Table
        const columns = [
            {
                title: 'City Name',
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => this.compare(a.name, b.name),
                
            }, 

            {
                title: 'Current Temp',
                dataIndex: 'temp',
                key: 'temp',
                sorter: (a, b) => this.compare(a.temp, b.temp),
                
            }, 

            {
                title: 'Today\'s Sunrise',
                dataIndex: 'sunrise',
                key: 'sunrise',
                sorter: (a, b) => this.compareTime(a.sunrise, b.sunrise)
                
            }, 

            {
                title: 'Today\'s Sunset',
                key: 'sunset',
                dataIndex: 'sunset',
                sorter: (a, b) => this.compareTime(a.sunset, b.sunset)
                
            }
        ];
        
        // constants for the Chart
        const dataForLabel = this.state.dataChart.map(item => {
                            return { ...item, label: item.y.toString() }
                        });
                        
        const chartWidth = 500;
        const chartHeight = 400;
        const chartDomain = [-30, 50];
        
        return(
            <div>
                <div>
                    <h1>Insert 3 Cities Names (in English)</h1>
                    <Form onSubmit={(this.state.input1 || this.state.input2 || this.state.input3)&&this.handleSearch.bind(this)}>
                        <Input onChange={(e)=>this.setState({input1:e.target.value})} value={input1} placeholder="City 1..."/>
                        <Input onChange={(e)=>this.setState({input2:e.target.value})} value={input2} placeholder="City 2..."/>
                        <Input onChange={(e)=>this.setState({input3:e.target.value})} value={input3} placeholder="City 3..."/>
                    </Form>
                    {<Button onClick={(this.state.input1 || this.state.input2 || this.state.input3)&&this.handleSearch.bind(this)}>Search</Button>}
                    {showAlert&&notification.open(args)}
                    <Button onClick={this.handleReset.bind(this)}>Clear</Button>
                </div>
                
                <Table columns={columns} dataSource={city} />
                {dataChart.length&&
                    <XYPlot xType="ordinal" width={chartWidth} height={chartHeight} yDomain={chartDomain}>
                        <XAxis />
                        <YAxis />
                        <VerticalBarSeries data={dataChart}/>
                        <LabelSeries
                            data={dataForLabel}
                            labelAnchorX="middle"
                            labelAnchorY="text-after-edge"
                        />
                    </XYPlot>
                } 
            </div>
        ); 
  }
}
        
ReactDOM.render(<Weather />, document.getElementById('root'));

serviceWorker.unregister();
